import {Component, OnInit} from '@angular/core';
import {User} from '../../models/User';
import {UserRowState} from '../../types/types';
import {UsersService} from '../../services/users.service';
import {HttpErrorResponse} from '@angular/common/http';
import {take} from 'rxjs/operators';
import {interval} from 'rxjs';


export interface UserRow {
  id: number;
  content: User;
  editContent: User;
  isEditable: boolean;
  ContentStatus: UserRowState;
}


@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  rows: UserRow[] = [];
  columns: string[] = ['ФИО', 'Дата рождения', 'Телефон'];
  errors: string[] = [];

  constructor(private usersService: UsersService) {
  }

  ngOnInit() {
    this.loadUsers();
  }


  setEditable(r: UserRow): void {
    r.isEditable = true;
    r.editContent = JSON.parse(JSON.stringify(r.content));
  }

  save(r: UserRow): void {
    r.isEditable = false;

    if (r.ContentStatus === UserRowState.Edit) {
      this.usersService.editUser({
        id: r.content.id,
        fio: r.editContent.fio,
        birthday: r.editContent.birthday,
        mobileNumber: r.editContent.mobileNumber
      }).subscribe(_ => {
        this.loadUsers();
      }, err => this.parseError(err));
    } else {
      this.usersService.addUser({
        fio: r.editContent.fio,
        birthday: r.editContent.birthday,
        mobileNumber: r.editContent.mobileNumber
      }).subscribe(_ => {
        r.ContentStatus = UserRowState.Edit;
        this.loadUsers();
      }, err => this.parseError(err));
    }
  }

  add(): void {
    this.rows.push({
      ContentStatus: UserRowState.New,
      content: {
        fio: '',
        birthday: '',
        mobileNumber: '',
        id: 0
      },
      isEditable: true,
      id: 0,
      editContent: {
        fio: '',
        birthday: '',
        mobileNumber: '',
        id: 0
      },
    });
  }

  validate(user: User): boolean {
    return ((user.fio !== '') && (user.birthday !== '') && (user.mobileNumber !== ''));
  }

  private loadUsers(): void {
    this.usersService.getAllUsers().subscribe(users => {
      const newrows = users.map(u => <UserRow>{
        id: u.id,
        content: u,
        IsEditable: false,
        ContentStatus: UserRowState.Edit
      });
      this.rows = [...newrows, ...this.rows.filter(r => r.ContentStatus === UserRowState.New)];
    });
  }

  private parseError(error: HttpErrorResponse): void {
    const birthdayError = error.error['Birthday'];
    this.errors.push(...birthdayError);

    interval(5000)
      .pipe(take(1))
      .subscribe(_ => this.errors = []);

  }

}
