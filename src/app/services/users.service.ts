import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/User';
import {environment} from '../../environments/environment';
import {EditUserCommand} from '../DTO/commands/EditUserCommand';
import {AddUserCommand} from '../DTO/commands/AddUserCommand';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.api}/users`);
  }

  editUser(user: EditUserCommand): Observable<null> {
    return this.http.put<null>(`${environment.api}/users`, user);
  }
  addUser(user: AddUserCommand): Observable<null> {
    return this.http.post<null>(`${environment.api}/users`, user);
  }

}
