export interface EditUserCommand {
  id: number;
  fio: string;
  mobileNumber: string;
  birthday: string;
}
