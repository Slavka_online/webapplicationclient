export interface AddUserCommand {
  fio: string;
  mobileNumber: string;
  birthday: string;
}
