import {Component, OnInit} from '@angular/core';
import {User} from './models/User';
import {UsersService} from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webApplicationClient';

  users: User[] = [];

  constructor(private usersService: UsersService) {}


  ngOnInit(): void {
      this.usersService.getAllUsers().subscribe(us => this.users = us);
  }



}
