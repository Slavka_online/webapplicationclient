export interface User {
  id: number;
  fio: string;
  mobileNumber: string;
  birthday: string;
}
